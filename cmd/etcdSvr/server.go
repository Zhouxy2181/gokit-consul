package main

import (
	"context"
	"fmt"
	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd/etcdv3"
	kitgrpc "github.com/go-kit/kit/transport/grpc"
	"gokit-consul/kitSvr"
	book "gokit-consul/proto"
	"google.golang.org/grpc"
	"log"
	"net"
	"time"
)

var (
	etcdServer  = "127.0.0.2:2379"
	prefix      = "/services/book/"
	instance    = "127.0.0.1:5001"
	serviceAddr = ":5001"
)

func main() {
	client, err := etcdv3.NewClient(context.Background(), []string{etcdServer}, etcdv3.ClientOptions{
		DialTimeout:   3 * time.Second,
		DialKeepAlive: 3 * time.Second,
	})
	if err != nil {
		log.Fatal("new client fail", err)
	}

	register := etcdv3.NewRegistrar(client, etcdv3.Service{
		Key:   prefix + instance,
		Value: instance,
	}, kitlog.NewNopLogger())
	register.Register()

	lis, err := net.Listen("tcp", serviceAddr)
	if err != nil {
		log.Fatal("listen fail", err)
	}
	svr := grpc.NewServer(grpc.UnaryInterceptor(kitgrpc.Interceptor))
	book.RegisterBookServiceServer(svr, kitSvr.NewServer())

	if err = svr.Serve(lis); err != nil {
		log.Fatal("conn fail", err)
	}
	fmt.Println("running......")
}
