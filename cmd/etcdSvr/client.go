package main

import (
	"context"
	"fmt"
	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/etcdv3"
	"github.com/go-kit/kit/sd/lb"
	book "gokit-consul/proto"
	"google.golang.org/grpc"
	"io"
	"log"
	"time"
)

func main() {
	listen, err := etcdv3.NewClient(context.Background(), []string{"127.0.0.2:2379"}, etcdv3.ClientOptions{
		DialTimeout:   3 * time.Second,
		DialKeepAlive: 3 * time.Second,
	})
	if err != nil {
		log.Fatal("listen fail", err)
	}

	instance, err := etcdv3.NewInstancer(listen, "/services/book/", kitlog.NewNopLogger())
	if err != nil {
		log.Fatal("instance fail", err)
	}

	endpoint := sd.NewEndpointer(instance, reqFactory, kitlog.NewNopLogger())
	banlance := lb.NewRoundRobin(endpoint)

	reqEndpoint, _ := banlance.Endpoint()
	req := struct{}{}
	if _, err = reqEndpoint(context.Background(), req); err != nil {
		log.Fatal("req fail", err)
	}

}

func reqFactory(instanceAddr string) (endpoint.Endpoint, io.Closer, error) {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		fmt.Println("请求服务: ", instanceAddr)
		conn, err := grpc.Dial(instanceAddr, grpc.WithInsecure())
		if err != nil {
			fmt.Println(err)
			panic("connect error")
		}
		defer conn.Close()
		bookClient := book.NewBookServiceClient(conn)
		bi, _ := bookClient.GetBookInfo(context.Background(), &book.BookParams{Id: 1})
		fmt.Println("获取书籍详情")
		fmt.Println("bookId: 1", " => ", "bookName:", bi.Name)

		bl, _ := bookClient.GetBookListInfo(context.Background(), &book.BookListParams{Page: 1, Limit: 10})
		fmt.Println("获取书籍列表")
		for _, b := range bl.BookList {
			fmt.Println("bookId:", b.Id, " => ", "bookName:", b.Name)
		}
		return nil, nil
	}, nil, nil
}
