package main

import (
	"context"
	"fmt"
	pb "gokit-consul/proto"
	"google.golang.org/grpc"
	"log"
	"net"
)

type BookSvr struct{}

func (t *BookSvr) GetBookInfo(ctx context.Context, in *pb.BookParams) (*pb.BookInfo, error) {
	bookinf := &pb.BookInfo{}
	bookinf.Id = in.Id
	bookinf.Name = "挪威的森林"

	return bookinf, nil
}
func (t *BookSvr) GetBookListInfo(ctx context.Context, in *pb.BookListParams) (*pb.BookListInfo, error) {
	booklist := &pb.BookListInfo{}
	booklist.BookList = append(booklist.BookList, &pb.BookInfo{Id: 1, Name: "挪威的森林"})
	booklist.BookList = append(booklist.BookList, &pb.BookInfo{Id: 2, Name: "海边的卡夫卡"})
	booklist.BookList = append(booklist.BookList, &pb.BookInfo{Id: 3, Name: "且听风吟"})

	return booklist, nil
}

func main() {
	serviceAddr := ":5001"
	lis, err := net.Listen("tcp", serviceAddr)
	if err != nil {
		log.Fatal("listen fail", err)
	}
	booksvr := new(BookSvr)

	grpcSvr := grpc.NewServer()
	pb.RegisterBookServiceServer(grpcSvr, booksvr)

	if err = grpcSvr.Serve(lis); err != nil {
		log.Fatal("run server fail", err)
	}
	fmt.Println("running......")
}
