package kitSvr

import (
	"github.com/go-kit/kit/endpoint"
	book "gokit-consul/proto"
)
import "context"

func MakeGetBookInfoEndpoint() endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*book.BookParams)
		bookInf := &book.BookInfo{}
		bookInf.Id = req.Id
		bookInf.Name = "挪威的森林"

		return bookInf, nil
	}
}

func MakeGetBookListInfoEndpoint() endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		booklistinf := &book.BookListInfo{}
		booklistinf.BookList = append(booklistinf.BookList, &book.BookInfo{Id: 1, Name: "挪威的森林"})
		booklistinf.BookList = append(booklistinf.BookList, &book.BookInfo{Id: 2, Name: "且听风吟"})
		booklistinf.BookList = append(booklistinf.BookList, &book.BookInfo{Id: 3, Name: "海边的卡夫卡"})

		return booklistinf, nil
	}
}

func DecodeRequest(_ context.Context, req interface{}) (interface{}, error) {
	return req, nil
}

func EncodeResponse(_ context.Context, req interface{}) (interface{}, error) {
	return req, nil
}
