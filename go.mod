module gokit-consul

go 1.13

require (
	github.com/go-kit/kit v0.10.0
	github.com/golang/protobuf v1.3.2
	google.golang.org/grpc v1.26.0
)
