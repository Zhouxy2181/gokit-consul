package main

import (
	"fmt"
	"gokit-consul/kitSvr"
	book "gokit-consul/proto"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	serverAddr := ":5001"
	lis, err := net.Listen("tcp", serverAddr)
	if err != nil {
		log.Fatal("listen fail", err)
	}

	svr := grpc.NewServer()
	book.RegisterBookServiceServer(svr, kitSvr.NewServer())

	if err = svr.Serve(lis); err != nil {
		log.Fatal("conn fail", err)
	}
	fmt.Println("running........")
}
