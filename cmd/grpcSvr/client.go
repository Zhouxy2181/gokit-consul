package main

import (
	"context"
	"fmt"
	book "gokit-consul/proto"
	"google.golang.org/grpc"
	"log"
)

func main() {
	serviceAddr := "127.0.0.1:5001"
	conn, err := grpc.Dial(serviceAddr, grpc.WithInsecure())
	if err != nil {
		log.Fatal("conn fail", err)
	}
	defer conn.Close()

	client := book.NewBookServiceClient(conn)
	bookInfo, err := client.GetBookInfo(context.Background(), &book.BookParams{Id: 1})
	if err != nil {
		log.Fatal("get book info fail", err)
	}
	fmt.Println("书籍详情")
	fmt.Println("num:", bookInfo.Id)
	fmt.Println("book name:", bookInfo.Name)
	booklist, err := client.GetBookListInfo(context.Background(), &book.BookListParams{Page: 1, Limit: 0})
	if err != nil {
		log.Fatal("get book list fail", err)
	}
	fmt.Println("书籍列表")
	for _, v := range booklist.BookList {
		fmt.Println("num:", v.Id, "----", "book name:", v.Name)
	}
}
