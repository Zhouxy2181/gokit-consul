package kitSvr

import (
	"context"
	"github.com/go-kit/kit/transport/grpc"
	book "gokit-consul/proto"
)

type BookServer struct {
	GetBookInfoHandler    grpc.Handler
	GetBookListInfHandler grpc.Handler
}

func (t *BookServer) GetBookInfo(ctx context.Context, in *book.BookParams) (*book.BookInfo, error) {
	_, rsp, err := t.GetBookInfoHandler.ServeGRPC(ctx, in)
	if err != nil {
		return nil, err
	}

	return rsp.(*book.BookInfo), nil
}

func (t *BookServer) GetBookListInfo(ctx context.Context, in *book.BookListParams) (*book.BookListInfo, error) {
	_, rsp, err := t.GetBookListInfHandler.ServeGRPC(ctx, in)
	if err != nil {
		return nil, err
	}

	return rsp.(*book.BookListInfo), nil
}

func NewServer() book.BookServiceServer {
	svr := &BookServer{}
	svr.GetBookInfoHandler = grpc.NewServer(
		MakeGetBookInfoEndpoint(),
		EncodeResponse,
		DecodeRequest,
	)
	svr.GetBookListInfHandler = grpc.NewServer(
		MakeGetBookListInfoEndpoint(),
		EncodeResponse,
		DecodeRequest,
	)

	return svr
}
